package org.yabloko.dataconsumer.domain.datamerger.view;

import java.util.List;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Obj {

  private String property;
  private List<String> list;
}