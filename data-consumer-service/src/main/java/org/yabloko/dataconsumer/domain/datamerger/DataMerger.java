package org.yabloko.dataconsumer.domain.datamerger;

import org.springframework.stereotype.Service;
import org.yabloko.dataconsumer.domain.datamerger.view.InputData;

@Service
public class DataMerger {

  public static MergedData mergeData(Data data, Data otherData, Data joinedData) {
    if (joinedData.getProperty().equals("non-mergable-data")) {
      throw new DataMergeException("Encountered data, that is not possible to merge.");
    }

    return MergedData.builder()
        .dataProperty(data.getProperty())
        .otherDataProperty(otherData.getProperty())
        .joinedDataProperty(joinedData.getProperty())
        .build();
  }

  public static String otherIdFromId(String id) {
    return String.format("otherData/%s", id);
  }

  public static String joinIdFromData(InputData inputDataOne, InputData inputDataTwo) {
    return inputDataTwo.getObject().getProperty();
  }
}
