package org.yabloko.dataconsumer.domain.datamerger;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Builder
@Getter
@EqualsAndHashCode
public class MergedData {

  private String dataProperty;
  private String otherDataProperty;
  private String joinedDataProperty;
}
