package org.yabloko.dataconsumer.domain.datamerger;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Data {

  private String property;
}
