package org.yabloko.dataconsumer.domain.datamerger.view;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class InputData {

  private String key;
  private Obj object;
}
