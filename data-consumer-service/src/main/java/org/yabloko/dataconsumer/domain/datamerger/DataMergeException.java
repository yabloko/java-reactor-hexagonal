package org.yabloko.dataconsumer.domain.datamerger;

public class DataMergeException extends RuntimeException {
  public DataMergeException(String message) {
    super(message);
  }
}
