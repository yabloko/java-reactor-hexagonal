package org.yabloko.dataconsumer.infra.http.gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.yabloko.dataconsumer.infra.http.gateway.dto.Data;
import reactor.core.publisher.Mono;

@Service
public class DataSourceGateway {

  WebClient webClient;

  public DataSourceGateway(@Autowired WebClient webClient) {
    this.webClient = webClient;
  }

  public Mono<Data> getData(String id) {
    return webClient
        .method(HttpMethod.GET)
        .uri(uriBuilder -> uriBuilder.path(String.format("datasource/%s", id)).build())
        .exchange()
        .flatMap(
            clientResponse -> {
              if(clientResponse.statusCode().equals(HttpStatus.OK)){
              return clientResponse.toEntity(Data.class).map(ResponseEntity::getBody);}
              else {
                throw new DataSourceException("HTTP response code is not OK.");
              }
            }
        ).onErrorMap(DataSourceException::new);
  }
}
