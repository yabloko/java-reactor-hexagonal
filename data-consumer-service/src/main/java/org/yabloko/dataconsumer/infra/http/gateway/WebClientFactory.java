package org.yabloko.dataconsumer.infra.http.gateway;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientFactory {
  @Bean
  public static WebClient getDataSourceWebClient() {
    return WebClient.create("http://localhost:8083");
  }
}
