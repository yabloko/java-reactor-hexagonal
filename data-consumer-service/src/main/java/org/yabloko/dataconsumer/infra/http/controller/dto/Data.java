package org.yabloko.dataconsumer.infra.http.controller.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@JsonAutoDetect
@Builder
public class Data {

  @JsonProperty
  private String property;
}
