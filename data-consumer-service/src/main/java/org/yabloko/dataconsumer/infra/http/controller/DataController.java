package org.yabloko.dataconsumer.infra.http.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.yabloko.dataconsumer.application.datamerger.ReactorDataMerger;
import org.yabloko.dataconsumer.infra.http.controller.dto.Data;
import reactor.core.publisher.Mono;

@RestController
public class DataController {

  @Autowired
  ReactorDataMerger reactorDataMerger;

  @GetMapping(value = "data/{id}", produces = "application/json")
  public Mono<ResponseEntity<Data>> getData(@PathVariable String id) {
    return reactorDataMerger.getData(id).map(
        mergedData -> new ResponseEntity<>(
            Data.builder()
                .property(
                    String.format(
                        "%s;%s;%s",
                        mergedData.getDataProperty(),
                        mergedData.getOtherDataProperty(),
                        mergedData.getJoinedDataProperty()
                    )
                )
                .build(),
            HttpStatus.OK
        )
    );
  }
}
