package org.yabloko.dataconsumer.infra.http.gateway;

public class DataSourceException extends RuntimeException {

  public DataSourceException(Throwable cause) {
    super(cause);
  }

  public DataSourceException(String message) {
    super(message);
  }
}
