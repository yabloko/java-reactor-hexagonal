package org.yabloko.dataconsumer.application.datamerger;

public class ReactorDataMergeException extends RuntimeException {
  public ReactorDataMergeException(Throwable cause) {
    super(cause);
  }
}
