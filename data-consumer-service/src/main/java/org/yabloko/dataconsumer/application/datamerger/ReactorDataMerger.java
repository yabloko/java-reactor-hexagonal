package org.yabloko.dataconsumer.application.datamerger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yabloko.dataconsumer.domain.datamerger.Data;
import org.yabloko.dataconsumer.domain.datamerger.DataMerger;
import org.yabloko.dataconsumer.domain.datamerger.MergedData;
import org.yabloko.dataconsumer.domain.datamerger.view.InputData;
import org.yabloko.dataconsumer.domain.datamerger.view.Obj;
import org.yabloko.dataconsumer.infra.http.gateway.DataSourceGateway;
import reactor.core.publisher.Mono;

@Service
public class ReactorDataMerger {

  //@TODO invert dependency here by declaring interface for Gateway
  DataSourceGateway dataSourceGateway;

  public ReactorDataMerger(
      @Autowired DataSourceGateway dataSourceGateway
  ) {
    this.dataSourceGateway = dataSourceGateway;
  }

  public Mono<MergedData> getData(String id) {
    final var dataMono = dataSourceGateway.getData(id);
    final var otherDataMono = dataSourceGateway.getData(DataMerger.otherIdFromId(id));

    return Mono.zip(dataMono, otherDataMono).flatMap(datas ->
        {
          final var joinedDataMono = dataSourceGateway
              .getData(
                  DataMerger.joinIdFromData(
                      inputDataFromGatewayData(datas.getT1()),
                      inputDataFromGatewayData(datas.getT2())
                  )
              );

          return joinedDataMono.map(
              joinedData -> {
                final var data = datas.getT1();
                final var otherData = datas.getT2();

                return DataMerger.mergeData(
                    Data.builder().property(data.getObject().getProperty()).build(),
                    Data.builder().property(otherData.getObject().getProperty()).build(),
                    Data.builder().property(joinedData.getObject().getProperty()).build()
                );
              }
          );
        }
    ).onErrorMap(ReactorDataMergeException::new);
  }

  private InputData inputDataFromGatewayData(
      org.yabloko.dataconsumer.infra.http.gateway.dto.Data dataOne
  ) {
    return InputData
        .builder()
        .key(dataOne.getKey())
        .object(Obj
            .builder()
            .property(dataOne.getObject().getProperty())
            .build())
        .build();
  }

}
