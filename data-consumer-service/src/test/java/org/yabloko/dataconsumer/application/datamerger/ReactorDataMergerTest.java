package org.yabloko.dataconsumer.application.datamerger;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Collections;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.yabloko.dataconsumer.domain.datamerger.DataMergeException;
import org.yabloko.dataconsumer.domain.datamerger.MergedData;
import org.yabloko.dataconsumer.infra.http.gateway.DataSourceException;
import org.yabloko.dataconsumer.infra.http.gateway.DataSourceGateway;
import org.yabloko.dataconsumer.infra.http.gateway.dto.Data;
import org.yabloko.dataconsumer.infra.http.gateway.dto.Obj;
import reactor.core.publisher.Mono;

public class ReactorDataMergerTest {

  ReactorDataMerger reactorDataMerger;
  DataSourceGateway dataSourceGateway;

  @Before
  public void setUp() {
    dataSourceGateway = Mockito.mock(DataSourceGateway.class);
    reactorDataMerger = new ReactorDataMerger(dataSourceGateway);
  }

  @Test
  public void itShouldMergeIfAllDataSourcesReturnedData() {
    final var dataKey = "data_key";
    final var otherDataKey = "otherData/data_key";
    final var joinDataKey = "join_data_key";

    final var expectedMergedData = MergedData
        .builder()
        .dataProperty("property_value")
        .otherDataProperty(joinDataKey)
        .joinedDataProperty("joined_property_value")
        .build();

    final var mockData = Data
        .builder()
        .key(dataKey)
        .object(
            Obj
                .builder()
                .property("property_value")
                .list(Collections.emptyList())
                .build())
        .build();

    final var mockOtherData = Data
        .builder()
        .key("other_data_key")
        .object(
            Obj
                .builder()
                .property(joinDataKey)
                .list(Collections.emptyList())
                .build())
        .build();

    final var mockJoinData = Data
        .builder()
        .key(joinDataKey)
        .object(
            Obj
                .builder()
                .property("joined_property_value")
                .list(Collections.emptyList())
                .build())
        .build();

    when(dataSourceGateway.getData(dataKey)).thenReturn(Mono.just(mockData));
    when(dataSourceGateway.getData(otherDataKey)).thenReturn(Mono.just(mockOtherData));
    when(dataSourceGateway.getData(joinDataKey)).thenReturn(Mono.just(mockJoinData));

    final var actualMergedData = reactorDataMerger.getData(dataKey).block();

    assertEquals(expectedMergedData, actualMergedData);
  }

  @Test(expected = ReactorDataMergeException.class)
  public void itShouldThrowIfOtherDataSourceThrows() {
    final var dataKey = "data_key";
    when(dataSourceGateway.getData(dataKey)).thenReturn(Mono.just(Data
        .builder()
        .build()));

    when(dataSourceGateway.getData("otherData/data_key")).thenReturn(
        Mono.error(new DataSourceException("Could not retrieve data from other source")));

    reactorDataMerger.getData(dataKey).block();
  }

  //interesting to check, if data call fails, other data call succeeds, and join data call is not mocked,
  // we can potentially hit NPE in test, as join data is not mocked
  //which is not happening - this test passes
  @Test(expected = ReactorDataMergeException.class)
  public void itShouldThrowIfDataSourceThrows() {
    final var dataKey = "data_key";
    when(dataSourceGateway.getData(dataKey)).thenReturn(
        Mono.error(new DataSourceException("Could not retrieve data from other source")
        ));
    when(dataSourceGateway.getData("otherData/data_key")).thenReturn(Mono.just(Data
        .builder()
        .build()));

    reactorDataMerger.getData(dataKey).block();
  }

  // should map exception to one that belongs to reactorDataMerger,
  // but this would make code too verbose
  @Test(expected = ReactorDataMergeException.class)
  public void itShouldThrowIfNotPossibleToMergeData() {
    final var dataKey = "data_key";
    final var otherDataKey = "otherData/data_key";
    final var joinDataKey = "join_data_key";

    final var mockData = Data
        .builder()
        .key(dataKey)
        .object(
            Obj
                .builder()
                .property("property_value")
                .list(Collections.emptyList())
                .build())
        .build();

    final var mockOtherData = Data
        .builder()
        .key("other_data_key")
        .object(
            Obj
                .builder()
                .property(joinDataKey)
                .list(Collections.emptyList())
                .build())
        .build();

    final var mockJoinData = Data
        .builder()
        .key(joinDataKey)
        .object(
            Obj
                .builder()
                .property("non-mergable-data")
                .list(Collections.emptyList())
                .build())
        .build();

    when(dataSourceGateway.getData(dataKey)).thenReturn(Mono.just(mockData));
    when(dataSourceGateway.getData(otherDataKey)).thenReturn(Mono.just(mockOtherData));
    when(dataSourceGateway.getData(joinDataKey)).thenReturn(Mono.just(mockJoinData));

    reactorDataMerger.getData(dataKey).block();
  }

}