package org.yabloko.dataconsumer.infra.http.controller;

import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.yabloko.dataconsumer.application.datamerger.ReactorDataMerger;

@TestConfiguration
public class ControllerTestConfiguration {

  @Bean
  public ReactorDataMerger reactorDataMerger() {
    return Mockito.mock(ReactorDataMerger.class);
  }
}
