package org.yabloko.dataconsumer.infra.http.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.yabloko.dataconsumer.application.datamerger.ReactorDataMerger;
import org.yabloko.dataconsumer.domain.datamerger.MergedData;
import org.yabloko.dataconsumer.infra.http.gateway.DataSourceException;
import reactor.core.publisher.Mono;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = {ControllerTestConfiguration.class})
@TestPropertySource(locations="classpath:test.properties")
public class DataControllerTest {
  @LocalServerPort
  private int port;

  @Autowired
  ReactorDataMerger reactorDataMerger;

  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  public void greetingShouldReturnMergedData() {
    final var dataProperty = "data";
    final var otherDataProperty = "other data";
    final var joinedDataProperty = "joined data";

    when(reactorDataMerger.getData(any()))
        .thenReturn(
            Mono.just(
                MergedData
                    .builder()
                    .dataProperty(dataProperty)
                    .otherDataProperty(otherDataProperty)
                    .joinedDataProperty(joinedDataProperty)
                    .build())
        );

  assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/data/one",
        String.class)).isEqualTo(String.format("{\"property\":\"%s;%s;%s\"}", dataProperty, otherDataProperty, joinedDataProperty));
  }

  @Test
  //@TODO This can be extended to check HTTP status as well
  public void greetingShouldReturnErrorMessageOnMergerError() {
    final var errorMessage = "Can't merge data!";

    when(reactorDataMerger.getData(any()))
        .thenReturn(
            Mono.error(new DataSourceException(errorMessage))
        );

    assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/data/one",
        String.class)).contains(errorMessage);
  }
}