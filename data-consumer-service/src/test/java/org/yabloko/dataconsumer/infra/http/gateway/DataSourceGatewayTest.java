package org.yabloko.dataconsumer.infra.http.gateway;

import static org.junit.Assert.assertEquals;

import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;
import java.util.List;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClient;
import org.yabloko.dataconsumer.infra.http.gateway.dto.Data;
import org.yabloko.dataconsumer.infra.http.gateway.dto.Obj;

public class DataSourceGatewayTest {

  @Test
  public void itShouldGetDataFromSource() {
    final var expectedKey = "one";
    final var expectedProperty = "property";
    final var expectedListItem1 = "item1";
    final var expectedListItem2 = "item2";
    final var expectedData = Data
        .builder()
        .key(expectedKey)
        .object(
            Obj
                .builder()
                .property(expectedProperty)
                .list(List.of(expectedListItem1, expectedListItem2))
                .build())
        .build();

    final var webServer = new MockWebServer();
    webServer.enqueue(new MockResponse()
        .setResponseCode(HttpStatus.OK.value())
        .setHeader("Content-Type", "application/json")
        .setBody(String.format(
            "{\"key\": \"%s\", \"object\": {\"property\": \"%s\", \"list\": [\"%s\", \"%s\"]}}",
            expectedKey, expectedProperty, expectedListItem1, expectedListItem2
        )));

    final var dataSourceGateway = new DataSourceGateway(
        WebClient.create(String.format("http://%s:%s", webServer.getHostName(), webServer.getPort())
        ));

    final var actualData = dataSourceGateway.getData("one").block();

    assertEquals(expectedData, actualData);
  }

  @Test(expected = DataSourceException.class)
  public void itShouldThrowIfCanNotDeserializeData() {
    final var webServer = new MockWebServer();

    final var  malformedJson = "{\"key\": \"one\"";
    webServer.enqueue(new MockResponse()
        .setResponseCode(HttpStatus.OK.value())
        .setHeader("Content-Type", "application/json")
        .setBody(malformedJson));

    final var dataSourceGateway = new DataSourceGateway(
        WebClient.create(String.format("http://%s:%s", webServer.getHostName(), webServer.getPort())
        ));

    dataSourceGateway.getData("one").block();
  }

  @Test(expected = DataSourceException.class)
  public void itShouldThrowIfServerReturnsNonHttpOk() {
    final var webServer = new MockWebServer();

    webServer.enqueue(new MockResponse()
        .setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
        .setHeader("Content-Type", "application/json"));

    final var dataSourceGateway = new DataSourceGateway(
        WebClient.create(String.format("http://%s:%s", webServer.getHostName(), webServer.getPort())
        ));

    dataSourceGateway.getData("one").block();
  }

  @Test(expected = DataSourceException.class)
  public void itShouldThrowIfContentTypeUnsupported() {
    final var webServer = new MockWebServer();

    webServer.enqueue(new MockResponse()
        .setResponseCode(HttpStatus.OK.value())
        .setBody("some body will be sent with text content-type"));

    final var dataSourceGateway = new DataSourceGateway(
        WebClient.create(String.format("http://%s:%s", webServer.getHostName(), webServer.getPort())
        ));

    dataSourceGateway.getData("one").block();
  }

}