package org.yabloko.dataconsumer.domain.datamerger;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.yabloko.dataconsumer.domain.datamerger.view.InputData;
import org.yabloko.dataconsumer.domain.datamerger.view.Obj;

public class DataMergerTest {

  @Test
  public void itShouldMergeValidData() {
    final var someDataProperty = "some data";
    final var someOtherDataProperty = "some other data";
    final var joinedDataProperty = "joined data";

    final var expectedMergedData = MergedData
        .builder()
        .dataProperty(someDataProperty)
        .otherDataProperty(someOtherDataProperty)
        .joinedDataProperty(joinedDataProperty)
        .build();

    final var data = Data.builder().property(someDataProperty).build();
    final var otherData = Data.builder().property(someOtherDataProperty).build();
    final var joinedData = Data.builder().property(joinedDataProperty).build();

    final var actualMergedData = DataMerger.mergeData(data, otherData, joinedData);

    assertEquals(expectedMergedData, actualMergedData);
  }

  @Test(expected = DataMergeException.class)
  public void itShouldThrowOnMergingInvlaidData() {
    final var someDataProperty = "some data";
    final var someOtherDataProperty = "some other data";
    final var joinedDataProperty = "non-mergable-data";

    final var expectedMergedData = MergedData
        .builder()
        .dataProperty(someDataProperty)
        .otherDataProperty(someOtherDataProperty)
        .joinedDataProperty(joinedDataProperty)
        .build();

    final var data = Data.builder().property(someDataProperty).build();
    final var otherData = Data.builder().property(someOtherDataProperty).build();
    final var joinedData = Data.builder().property(joinedDataProperty).build();

    DataMerger.mergeData(data, otherData, joinedData);
  }

  @Test
  public void itShouldDeriveOtherIdFromId() {
    final var id = "data_id";
    final var expectedOtherId = "otherData/data_id";
    final var actualOtherId = DataMerger.otherIdFromId(id);

    assertEquals(expectedOtherId, actualOtherId);
  }

  @Test
  public void itShouldDeriveJoinPropertyFromInputDatas() {
    final var inputDataOne = InputData.builder().build();
    String expectedJoinProperty = "join_property";
    final var inputDataTwo = InputData
        .builder()
        .object(
            Obj.builder().property(expectedJoinProperty).build()
        ).build();

    final var actualJoinProperty = DataMerger.joinIdFromData(inputDataOne, inputDataTwo);

    assertEquals(expectedJoinProperty, actualJoinProperty);
  }

}