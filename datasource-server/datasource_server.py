#!/usr/bin/env python3
import json

from flask import Flask, request, Response

app = Flask(__name__)

@app.route('/datasource/one', methods=['GET'])
def handler_one():
    response_body = {"key": "one", "object": {"property": "data", "list": ["item1", "item2"]}}
    outgoing_response = {
        "status": 200,
        "headers": {"Content-Type": "application/json "},
        "body": json.dumps(response_body)
    }

    return Response(
        outgoing_response["body"],
        status=outgoing_response["status"],
        headers=outgoing_response["headers"]
    )

@app.route('/datasource/otherData/one', methods=['GET'])
def handler_other():
    response_body = {"key": "otherData", "object": {"property": "joinedByOtherData", "list": ["item1", "item2"]}}
    outgoing_response = {
        "status": 200,
        "headers": {"Content-Type": "application/json "},
        "body": json.dumps(response_body)
    }

    return Response(
        outgoing_response["body"],
        status=outgoing_response["status"],
        headers=outgoing_response["headers"]
    )

@app.route('/datasource/joinedByOtherData', methods=['GET'])
def handler_joined():
    response_body = {"key": "joinedByOtherData", "object": {"property": "lol hello there", "list": ["item1", "item2"]}}
    outgoing_response = {
        "status": 200,
        "headers": {"Content-Type": "application/json "},
        "body": json.dumps(response_body)
    }

    return Response(
        outgoing_response["body"],
        status=outgoing_response["status"],
        headers=outgoing_response["headers"]
    )

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8083)