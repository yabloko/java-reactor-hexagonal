Example of how Reactor (projectreactor.io) and be used with Spring2 application, and project organized in Hexagonal fashion.

How to run:

Run data source server with python:
```$xslt
> cd datasource-server
> pip3 install -r requirements.txt
```
Run data consumer app, that merges data from different sources:
```$xslt
> cd data-consumer-app
> mvn clean package
> java -jar target/data-consumer-app-0.0.1-SNAPSHOT.jar
```
Request merged data:
```$xslt
> curl -X GET \
    http://localhost:8080/data/one
```


This project is an example using Reactor and Spring in Ports and Adaptors fashion:

high-level packages are "application", "domain" and "infra"

All code related to Http is in Infra namespace - both exposed controllers and gateways to datasources;

Application layer represents, how basic domain capabilities are combined (this is where pure Reactor is used)

Domain contains basic capabilities of app - logic of merging data from different sources, along with data validation etc.
As an example, it throws if there is pre-defined "unmergable" data.

Components of each layer are tested with approach inspired by https://martinfowler.com/articles/microservice-testing/

Domain layer contains pure Java, and is tested by classic Unit tests.

Application layer is combining components and is tested with help of Infra Mocks, but uses real Domain implementation.

Infra layer is tested with Mock of Application layer and real infrastructural counterpart - web server or client.